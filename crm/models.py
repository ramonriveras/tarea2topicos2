from django.conf import settings
from django.db import models

class Client(models.Model):
    name = models.CharField("Nombre",max_length=144)
    businessName = models.CharField("Razon social",max_length=144)
    address = models.CharField("Direccion",max_length=144)
    email = models.EmailField("Email", unique=True, default=False)
    identificactionType = models.CharField("Tipo de identificacion",default=False, max_length=20, choices=settings.INDENTIFICATION_CHOICES)
    numero = models.CharField("Numero identificacion:", max_length=20)
    status = models.BooleanField("Usuario activo?", default=False)
    tecnicalContact = models.EmailField("Email contacto tecnico", unique=True, default=False)
    comercialContact = models.EmailField("Email contacto comercial", unique=True, default=False)
    financeContact = models.EmailField("Email contacto financiero", unique=True, default=False)
    
    sort_order = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class ClientService(models.Model):
    name = models.CharField("Nombre del servicio",max_length=144)
    coment = models.TextField("Comentario",max_length=500)
    typeSla = models.CharField("Tipo de SLA",default=False, max_length=20, choices=settings.TYPE_SLA)
    typeContract = models.CharField("Tipo de contrato",default=False, max_length=20, choices=settings.TYPE_CONTRACT)
    
    sort_order = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


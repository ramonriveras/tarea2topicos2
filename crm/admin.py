from django.contrib import admin
from crm.models import Client, ClientService


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'name', 
    )

@admin.register(ClientService)
class ClientServiceAdmin(admin.ModelAdmin):
    list_display = (
        'name', 
       
    )



from django.shortcuts import render
from crm.models import Client


def home(request):
    template_name = 'index.html'
    data = {}
    # select * from blog WHERE status = 1
    data['name'] = Client.objects.filter(status=True) 

    return render(request, template_name, data)
